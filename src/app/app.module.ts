import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

// Pages
import { HomePage } from '../pages/home/home';
import { SurveyPage } from '../pages/survey/survey';
import { CompletionPage } from '../pages/survey/completion';
import { ContactCorrectionPage } from '../pages/home/contact-correction';
import { CompletionCheckPage } from '../pages/survey/completion-check';
import { SurveyInformationPage } from '../pages/home/survey-information';
import { ShareInfoPage } from '../pages/survey/share-info';
import { ErrorPage } from '../pages/error/error';

// Services
import { SurveyService } from '../services/survey-service';
import { HelperService } from '../services/helpers';

// Components
import { QuestionSlideComponent } from '../pages/survey/components/slide.comonent';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SurveyInformationPage,
    SurveyPage,
    CompletionCheckPage,
    CompletionPage,
    ContactCorrectionPage,
    ShareInfoPage,
    ErrorPage,

    QuestionSlideComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SurveyInformationPage,
    SurveyPage,
    CompletionCheckPage,
    CompletionPage,
    ContactCorrectionPage,
    ShareInfoPage,
    ErrorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SurveyService,
    HelperService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
