import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { HomePage } from '../pages/home/home';
import { ErrorPage } from '../pages/error/error'

// Services
import { SurveyService } from '../services/survey-service';
import { HelperService } from '../services/helpers';
import { Config } from '../services/config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(
    platform: Platform, 
    statusBar: StatusBar,
    public helperService: HelperService,
    public surveyService: SurveyService
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      // Check for access key.
      this.checkForAccessKey();
    });
  }

  checkForAccessKey()
  {
    // See if we have a key.
    let urlParams = this.helperService.getURLParameters();
    let foundKey:boolean = false;
    for ( let p of urlParams )
    {
      if ( p.name === 'key' && p.value.length > 0 )
      {
        foundKey = true;
        Config.ACCESS_KEY = p.value;
      }
    }

    if ( foundKey === true )
    {
      // Save in local storage.
      this.saveKey();

      // Then see is we can get a valid survey.
      this.getSurveyWithKey();
    } else {
      let errCode = 'no access key';
      this.showErrorPage( errCode );
    }
  }

  saveKey()
  {
    localStorage.setItem( 'access_key', Config.ACCESS_KEY ); 
  }

  getSurveyWithKey()
  {
    this.surveyService.getSurveyQuestions()
      .subscribe( data => {
        Config.SURVEY_OBJECT = data;

        // TODO: If no survey data found, throw an error.
        if ( data.Questions.length === 0 )
        {
          Config.ERROR_CODE = 'no survey found';
          this.rootPage = ErrorPage;
        } else {
          this.rootPage = HomePage;
        }
      });
  }

  showErrorPage( errCode )
  {
    Config.ERROR_CODE = errCode;
    this.rootPage = ErrorPage;
  }
}

