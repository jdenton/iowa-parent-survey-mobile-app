import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {

  constructor(  ) {}

  getURLParameters()
  {
    let urlParameters = [];
    if ( document.URL.indexOf("?") > 0 ) {
      let splitURL = document.URL.split("?");
      let splitParams = splitURL[1].split("&");
      let i: any;
      for ( i in splitParams ) 
      {
        let singleURLParam = splitParams[i].split('=');
        let urlParameter = {
          'name': singleURLParam[0],
          'value': singleURLParam[1]
        };
        urlParameters.push(urlParameter);
      }
    }

    return urlParameters;
  }
}