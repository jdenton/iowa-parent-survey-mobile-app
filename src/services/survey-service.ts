import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../services/config';

// Headers for form-data POST.
/*
const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
  })
}
*/

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveyService {
  data: any;

  constructor( 
    public http: HttpClient
  ) {}

  getSurveyQuestions()
  {
    return this.http.get( Config.API_ROOT + 'access/' + Config.ACCESS_KEY )
    .pipe( map( this.processData, this ));
  }

  saveSurveyResponse( response, question )
  {
    let responseObj = {
      QuestionId: question.QuestionId,
      UserId: Config.ACCESS_KEY,
      Response: response
    };
    /*
    let responseObj = new FormData();
    responseObj.append( 'UserId', Config.ACCESS_KEY );
    responseObj.append( 'QuestionId', question.QuestionId );
    responseObj.append( 'Response', response );
    */
    return this.http.post( Config.API_ROOT + 'responses', responseObj, httpOptions);
  }

  submitSurvey( submitObj )
  {
    let submitObjPost = {
      UserId: Config.ACCESS_KEY,
      UserComments: submitObj.userComments,
      ShareStoryFlag: submitObj.shareStoryFlag,
      UserName: submitObj.userName,
      UserEmail: submitObj.userEmail,
      UserPhone: submitObj.userPhone
    };
    /*
    let submitObjPost = new FormData();
    submitObjPost.append( 'UserId', Config.ACCESS_KEY );
    submitObjPost.append( 'UserComments', submitObj.userComments );
    submitObjPost.append( 'ShareStoryFlag', submitObj.shareStoryFlag );
    submitObjPost.append( 'UserName', submitObj.userName );
    submitObjPost.append( 'UserEmail', submitObj.userEmail );
    submitObjPost.append( 'UserPhone', submitObj.userPhone );
    */
    return this.http.post( Config.API_ROOT + 'submit', JSON.stringify(submitObjPost), httpOptions);
  }

  resetSurvey()
  {

  }

  processData(data: any) 
  {
    this.data = data;

    return this.data;
  } 
}  