export let Config = {
  APP_VERSION: '1.0.0',
  API_ROOT: 'https://www.edinfo.state.ia.us/I-Star/istarservices.ashx/survey/familysurvey/',
  ACCESS_KEY: '',
  APP_NAME: 'iowa-parent-survey',
  PLATFORM_WIDTH: 0,
  USER: {},
  SURVEY_OBJECT: {},
  ERROR_CODE: ''
};