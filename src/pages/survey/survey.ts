import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ElementRef, ViewChild } from '@angular/core';
import { Content, Slides } from 'ionic-angular';

// Services
import { SurveyService } from '../../services/survey-service';

// Pages
import { CompletionCheckPage } from './completion-check';

// Config
import { Config } from '../../services/config';

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html'
})
export class SurveyPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;

  questions:any = [];
  responseButtons:any = [
    {
      label: 'Not at all helpful',
      selected: false,
      badgeColor: '#c2bdfd',
      badgeText: '1'
    },{   
      label: 'A little helpful',
      selected: false,
      badgeColor: '#cfc6ee',
      badgeText: '2'
    },{
      label: 'Somewhat helpful',
      selected: false,
      badgeColor: '#e0d4d7',
      badgeText: '3'
    },{
      label: 'Very helpful',
      selected: false,
      badgeColor: '#f3e2c2',
      badgeText: '4'
    },{    
      label: 'Extremely helpful',
      selected: false,
      badgeColor: '#FFECB2',
      badgeText: '5'
    },{
      label: 'Not applicable',
      selected: false
    }
  ];

  questionCount:number = 0;
  questionResponsesCount:number = 0;
  completionPercentage:string = '0';
  currentSectionIndex:number = 0;
  selectedButtonIndex:number = null;
  currentQuestionIndex:number = 0;
  currentQuestionNumber:number = 1;
  firstQuestion:boolean = true;
  lastQuestion:boolean = false;
  numberIncomplete:number = 0;
  sectionTitle:string = '';

  constructor(
    public navCtrl: NavController,
    public surveyService: SurveyService
  ) {
    if ( this.questions.length === 0 )
    {
      // Load our survey questions.
      this.getSurvey();
    }
  }
  
  getSurvey()
  {
    let surveyObj:any = Config.SURVEY_OBJECT;
    this.questions = surveyObj.Questions;
    this.questionCount = this.questions.length;
    this.numberIncomplete = this.questionCount;
    this.sectionTitle = this.questions[0].Section;
    this.checkProgress( true );
  }

  //
  // checkProgress calculates where we are in the 
  // completion of a survey.
  //
  checkProgress( firstPass = false )
  {
    if ( firstPass === true )
    {
      for ( let q of this.questions )
      {
        if ( q.Response.length > 0 )
        {
          // Get selected button index from response.
          let i = 0;
          for ( let b of this.responseButtons )
          {
            if ( b.label === q.Response )
            {
              q.response = true;
              q.selectedButtonIndex = i;
            }
            i++;
          }
        }
      }
    }
  }

  saveResponse( response, question, buttonIndex )
  {    
    console.log(response);
    this.surveyService.saveSurveyResponse( response, question )
      .subscribe( data => {
        this.questionResponsesCount = 0;
        this.selectedButtonIndex = buttonIndex;
        
        // Mark question complete in local survey object.
        for ( let q of this.questions )
        {
          if ( q.response === true )
          {
            this.questionResponsesCount++;
          }
          if ( q.QuestionId === question.QuestionId )
          {
            q.response = true;
            q.selectedButtonIndex = buttonIndex;
            this.questionResponsesCount++;
          }
        }

        this.completionPercentage = (( this.questionResponsesCount / this.questionCount ) * 100 ).toFixed(0);
        this.numberIncomplete = this.questionCount - this.questionResponsesCount;

        // Have we completed all the questions?
        //if ( this.slides.isEnd() )
        if ( this.slides.isEnd() || this.questionResponsesCount === this.questionCount )
        {
          setTimeout(() => {
            this.navCtrl.push( CompletionCheckPage, {
              incomplete: this.numberIncomplete
            });
          }, 1000);
        } else {
          setTimeout(() => {
            // TODO: Advance to next unanswered question.
            // Normally this will simply be the next slide.
            // However, when going back and answering questions not 
            // answered the first time through, the next slide
            // could actually be serveral questions ahead.
            // slideIndex === questionIndex
            this.slides.slideTo( this.findNextUnansweredQuestion(), 1000 );
          }, 1400);
        }
      });
  }

  // Loop through all questions, find next
  // unanswered question with index > currentSlideIndex.
  findNextUnansweredQuestion()
  {
    let unansweredQuestionIndex = 0;

    for ( let q of this.questions )
    {
      if ( q.response !== true && unansweredQuestionIndex >= this.slides.getActiveIndex() )
      {
        return unansweredQuestionIndex;
      }
      unansweredQuestionIndex++;
    }
  }

  slideWillChange()
  {
    this.selectedButtonIndex = null;
  }

  slideChanged()
  {
    let questionIndex = this.slides.getActiveIndex();

    if ( typeof this.questions[questionIndex] !== 'undefined' )
    {
      this.currentQuestionNumber = questionIndex + 1;
      this.selectedButtonIndex = this.questions[questionIndex].selectedButtonIndex;
      this.sectionTitle = this.questions[questionIndex].Section;
    

      if ( questionIndex > 0 )
      {
        this.firstQuestion = false;
        this.lastQuestion = false;
      }
      if ( questionIndex === this.questionCount - 1 )
      {
        this.lastQuestion = true;
      }
      if ( questionIndex === 0 )
      {
        this.firstQuestion = true;
      }    
    } 
  }

  prevSlide()
  {
    this.slides.slidePrev();
  }

  nextSlide()
  {
    if ( this.lastQuestion )
    {
      this.navCtrl.push( CompletionCheckPage, {
        incomplete: this.numberIncomplete
      });
    }
    this.slides.slideNext();
  }

  //
  // Button style functions
  //
  isButtonOutline( buttonIndex )
  {
    if ( buttonIndex === this.selectedButtonIndex )
    {
      return false;
    }
    return true;
  }

  buttonColor( buttonIndex )
  {
    if ( buttonIndex === this.selectedButtonIndex )
    {
      return 'success';
    }
    return 'muted';
  }

  buttonIsSelected( buttonIndex )
  {
    if ( buttonIndex === this.selectedButtonIndex )
    {
      return true;
    }
    return false;
  }

  ionViewDidEnter()
  {
    // Set slide index to 0
    this.slides.slideTo( 0, 0 );
    // Prevent swipe to next slide but 
    // allow PREV/NEXT buttons to work.
    this.slides.onlyExternal = true;

    // Now, move to first unanswered question slide.
    this.slides.slideTo( this.findNextUnansweredQuestion(), 1000 );
  }
}
