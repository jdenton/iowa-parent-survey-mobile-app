import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-completion',
  templateUrl: 'completion.html'
})
export class CompletionPage {
  constructor(
    public navCtrl: NavController
  ) {

  }
}
