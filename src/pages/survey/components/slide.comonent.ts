import { 
  Component, 
  EventEmitter, 
  Input, 
  Output,
  ViewChild
} from '@angular/core';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'question-slide-component',
  templateUrl: 'slide.component.html'
})

//
// QuestionSlideComponent (child) communicates with the 
// Survey (parent) through EventEmitter.
// This seems ok because the parent should be in charge of which
// slide deck (questions) is currently being displayed.
//
export class QuestionSlideComponent
{
  // Inputs
  @Input() questions:any = [];
  @Input() metaQuestion:string = '';
  @Input() responses:any = [];

  // Outputs
  @Output() slideChange = new EventEmitter<string>();
  @Output() saveResponse = new EventEmitter<string>();
  @Output() slideEnd = new EventEmitter<string>();

  @ViewChild(Slides) slides: Slides;

  selectedButtonIndex:number = null;

  constructor() {
    
  }

  _slideChange( ev )
  {
    this.selectedButtonIndex = null;
  }

  _saveResponse( response, q, j )
  {
    let responseObj:any = {
      response: response,
      question: q,
      index: j
    };
    this.saveResponse.emit( responseObj );
    this.selectedButtonIndex = j;

    // Move to next slide
    if ( this.slides.isEnd() )
    {
      this.slideEnd.emit();
    } else {
      setTimeout(() => {
        this.slides.slideNext();
      }, 500);
    }
  }

  //
  // Button style functions
  //
  _isButtonOutline( buttonIndex )
  {
    if ( buttonIndex === this.selectedButtonIndex )
    {
      return false;
    }
    return true;
  }

  _buttonColor( buttonIndex )
  {
    if ( buttonIndex === this.selectedButtonIndex )
    {
      return 'success';
    }
    return 'muted';
  }

  ngOnInit()
  {
  }
}