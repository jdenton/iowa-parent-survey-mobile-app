import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Pages
import { CompletionPage } from './completion';

// Services
import { SurveyService } from '../../services/survey-service'; 

@Component({
  selector: 'page-share-info',
  templateUrl: 'share-info.html'
})
export class ShareInfoPage {
  // Form fields
  userComments: string = '';
  userName: string = '';
  userEmail: string = '';
  userPhone: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public surveyService: SurveyService
  ) {
    this.userComments = this.navParams.get( 'userComments' );
    console.log(this.userComments);
  }

  submitSurvey()
  {
    let submitObj = {
      userComments: this.userComments,
      shareStoryFlag: true,
      userName: this.userName,
      userEmail: this.userEmail,
      userPhone: this.userPhone
    };
    this.surveyService.submitSurvey( submitObj ).subscribe( data => {
      console.log(data);
      this.navCtrl.setRoot( CompletionPage );
    });
  }

  showSurveyPage()
  {
    this.navCtrl.pop();
  }
}
