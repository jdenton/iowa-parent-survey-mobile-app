import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Pages
import { CompletionPage } from './completion';
import { ShareInfoPage } from './share-info';
import { SurveyPage } from '../survey/survey';

// Services
import { SurveyService } from '../../services/survey-service'; 

@Component({
  selector: 'page-completion-check',
  templateUrl: 'completion-check.html'
})
export class CompletionCheckPage {
  incomplete:number = 0;
  pageTitle:string = 'Submit Survey';

  // Form fields
  userComments: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public surveyService: SurveyService
  ) {
    this.incomplete = navParams.get('incomplete');
    if ( this.incomplete > 0 )
    {
      this.pageTitle = 'Check Responses';
    }
  }

  submitSurvey()
  {
    let submitObj = {
      userComments: this.userComments,
      shareStoryFlag: false
    };
    this.surveyService.submitSurvey( submitObj ).subscribe( data => {
      console.log(data);
      this.navCtrl.setRoot( CompletionPage );
    });
  }

  showSurveyPage()
  {
    this.navCtrl.pop();
  }

  showShareInfoPage()
  {
    this.navCtrl.push( ShareInfoPage, {
      userComments: this.userComments
    });
  }

  showSurveyCompletionMessage()
  {
    this.incomplete = 0;
    this.pageTitle = 'Submit Survey';
  }
}
