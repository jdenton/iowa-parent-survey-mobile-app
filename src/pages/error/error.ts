import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Config
import { Config } from '../../services/config';

@Component({
  selector: 'page-error',
  templateUrl: 'error.html'
})
export class ErrorPage {
  errorCode: string = '';
  errorMessage: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.errorCode = Config.ERROR_CODE;
    this.displayErrorMessage();
  }

  displayErrorMessage()
  {
    switch ( this.errorCode )
    {
      case 'no access key':
        this.errorMessage = 'No access key was provided.';
        break;
      case 'no survey found':
        this.errorMessage = 'No survey was found for the access key provided.';
        break;
      default:
        this.errorMessage = 'There was some kind of error.';
    }
  }
}