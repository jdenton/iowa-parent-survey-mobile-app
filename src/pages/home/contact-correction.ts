import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-contact-correction',
  templateUrl: 'contact-correction.html'
})
export class ContactCorrectionPage {
  submitted:boolean = false;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController
  ) {

  }

  submitContactInformation()
  {
    this.submitted = true;

    let toast = this.toastCtrl.create({
      message: 'Your contact information was submitted.',
      duration: 2000,
      position: 'bottom',
      cssClass: 'toast-success',
      showCloseButton: false
    });

    toast.present();

    this.navCtrl.pop();
  }
}
