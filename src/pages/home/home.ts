import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Pages
import { SurveyPage } from '../survey/survey';
import { SurveyInformationPage } from '../home/survey-information';
import { CompletionCheckPage } from '../survey/completion-check';
import { Config } from '../../services/config';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  infoIsCorrect:boolean = false;
  coordinatorData: any = {};

  constructor(
    public navCtrl: NavController
  ) 
  {
    let surveyObj:any = Config.SURVEY_OBJECT;
    this.coordinatorData = surveyObj.CoordinatorData;
  }

  isInfoCorrect()
  {
    return this.infoIsCorrect;
  }

  showSurveyPage()
  {
    this.navCtrl.setRoot( SurveyPage );
  }

  showSurveyAboutPage()
  {
    this.navCtrl.push( SurveyInformationPage );
  }

  goToCompletionPage()
  {
    this.navCtrl.push( CompletionCheckPage, {
      incomplete: 0
    });
  }
}
