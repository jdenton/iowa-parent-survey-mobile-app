import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Pages
import { SurveyPage } from '../survey/survey';

@Component({
  selector: 'page-survey-information',
  templateUrl: 'survey-information.html'
})
export class SurveyInformationPage {
  infoIsCorrect:boolean = false;

  constructor(
    public navCtrl: NavController
  ) {

  }

  showSurveyPage()
  {
    this.navCtrl.setRoot( SurveyPage );
  }
}
